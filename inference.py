import tensorflow as tf
from train_model import inference, optimizer, encoder, decoder, inp_que, targ_ans, max_length_inp, max_length_targ, checkpoint

checkpoint_dir = './training_checkpoints'
# restoring the latest checkpoint in checkpoint_dir
checkpoint.restore(tf.train.latest_checkpoint(checkpoint_dir))

print(inference('hello', encoder, decoder, inp_que, targ_ans, max_length_inp, max_length_targ))